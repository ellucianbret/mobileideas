//
//  ProximityDelegate.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/5/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

// Simple protocol view controllers can implement to recieve updates on beacons they are interested in.
// ViewControllers pass in the UUID they are interested in listening too (optionally pass in major/minor too)
// BeaconManager then calls the appropriate delegate(s) that are listening for updates.

import Foundation
import CoreLocation

@objc
protocol ProximityChangeDelegate {
    // Called when a beacon's proximity changes
    optional func proximityUpdate(beacon: CLBeacon)
    
    // Called when a beacon comes in Range
    optional func proximityNowInRange(beacon: CLBeacon)
    
    // Called when a beacon goes out of Range
    optional func proxmityNowOutOfRange(beacon: Beacon)
}