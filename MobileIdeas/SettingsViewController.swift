//
//  SettingsViewController.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/13/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var attendeeName: UITextField!
    
    struct const {
        static let namesSetPreferenceKey: String = "names_set_preference"
        static let namePreferenceKey: String = "name_preference"
    }
    
    var alreadySetNames = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        attendeeName.delegate = self;
    }
    
    // MARK: - UITextFieldDelegate Methods
    func textFieldShouldReturn(textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        performSegueWithIdentifier("mainview", sender: self)

        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if ( attendeeName.isFirstResponder()) {
            self.view.endEditing(true)
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    
        let attendeeName = self.attendeeName.text.isEmpty ? "Anonymous User" : self.attendeeName.text
        
        println( "The name is \(attendeeName)")

        
        // We haven't already set the names into the settings. Let's do so now.
        if !alreadySetNames {
            let defaults = NSUserDefaults.standardUserDefaults()
            
            defaults.setObject(attendeeName, forKey: const.namePreferenceKey)
            
            // Remember that we have already set the attendee name.
            defaults.setBool(Bool(true), forKey: const.namesSetPreferenceKey )
            
            defaults.synchronize() // Ensures the values will be updated in the settings
        }
    
    }
}
