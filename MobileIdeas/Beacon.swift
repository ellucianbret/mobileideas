//
//  Beacon.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/4/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit
import CoreLocation

class Beacon: NSObject {
    var uuid: String
    var major: NSNumber
    var minor: NSNumber
    var found: Bool {
        didSet {
            // Store in the prefs if the beacon has been found.
            if (self.found) {
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(self.found, forKey: self.id())
                defaults.synchronize()
            }
        }
    }
    var name: String
    var title: String
    var photoName: String
    var content: String
    var proximity: CLProximity = CLProximity.Unknown
    var lastRanged: NSDate?
    var lastNotified: NSDate?
    
    
    init(uuid: String, major: Int, minor: Int, found: Bool, name: String, title: String, photoName: String, content: String) {
        self.uuid = uuid
        self.major = major
        self.minor = minor
        self.found = found
        self.name = name
        self.title = title
        self.photoName = photoName
        self.content = content
        self.proximity = CLProximity.Unknown
        
        super.init()
        
    }
    
    init(uuid: String, major: Int, minor: Int, proximity: CLProximity) {
        self.uuid = uuid
        self.major = major
        self.minor = minor
        self.found = false
        self.name = ""
        self.title = ""
        self.photoName = ""
        self.content = ""
        self.proximity = proximity

        super.init()
        
    }

    init(uuid: String, major: Int, minor: Int) {
        self.uuid = uuid
        self.major = major
        self.minor = minor
        self.found = false
        self.name = ""
        self.title = ""
        self.photoName = ""
        self.content = ""
        self.proximity = CLProximity.Unknown
        
        super.init()
    }
    
    func id() -> String {
    
        return "\(uuid.uppercaseString).\(major).\(minor)"
    }
    
    class func id(beacon: CLBeacon) -> String {
        
        return "\(beacon.proximityUUID.UUIDString.uppercaseString).\(beacon.major).\(beacon.minor)"
    }
}