//
//  EliveSession.swift
//  MobileIdeas
//
//  Created by Jay Glose on 3/8/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation

class EliveSession: NSObject {
    var title: String
    var shortTitle: String
    var sessionId: Int
    var location: String
    var sessionDesc: String
    var sessionDateTime: NSDate
    
    init(title: String, shortTitle: String?=nil, sessionId: Int, location: String, sessionDesc: String,
        sessionDateTime: NSDate) {
        self.title = title
        self.shortTitle = shortTitle != nil ? shortTitle! : title
        self.sessionId = sessionId
        self.location = location
        self.sessionDesc = sessionDesc
        self.sessionDateTime = sessionDateTime
        super.init()
    }
}