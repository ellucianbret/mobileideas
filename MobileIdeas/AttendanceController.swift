//
//  SecondViewController.swift
//  MobileIdeas
//
//  Created by Bret Hansen on 2/18/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit

class AttendanceController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var attendanceLabel: UILabel!
    @IBOutlet weak var sessionButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var sessionTitleButton: UIButton!
    @IBOutlet weak var attendanceTableView: UITableView!
    
    let iso8601Formatter = NSDateFormatter()
    let displayDateFormater = NSDateFormatter()
    
    var refreshTimer: NSTimer?
    
    var currentSession: EliveSession?
    var currentSessionPickedTime: NSDate?
    var userChoosenSession = false
    
    var attendanceData: NSArray?
    var attendanceList: NSArray?

    override func viewDidLoad() {
        super.viewDidLoad()

        iso8601Formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        iso8601Formatter.timeZone = NSTimeZone(name: "UTC")

        displayDateFormater.dateStyle = NSDateFormatterStyle.ShortStyle
        displayDateFormater.timeStyle = NSDateFormatterStyle.ShortStyle
    }
    
    override func viewWillAppear(animated: Bool) {
        sessionButton.setTitle("", forState: UIControlState.Normal)
        countLabel.text = ""
        sessionTitleButton.setTitle("", forState: UIControlState.Normal)
        locationLabel.text = ""
    }
    
    override func viewDidAppear(animated: Bool) {
        startRefreshDataWithInterval(30.0)
        userChoosenSession = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        stopRefreshData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeSession(sender: UIButton) {
        if attendanceData != nil {
            // need to show an action sheet of existing sessions with data
            var sessionIds = [Int]()
            for attendance in attendanceData! {
                let sessionId = attendance["sessionId"] as Int
                if !contains(sessionIds, sessionId) {
                    sessionIds.append(sessionId)
                }
            }
            sort(&sessionIds)
            
            let alertController = UIAlertController(title: "Choose a Session", message: nil, preferredStyle: .ActionSheet)
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
                //Just dismiss the alert
            }
            alertController.addAction(cancelAction)
            
            let sessionsById = SessionManager.instance.sessionsById
            for sessionId in sessionIds {
                let session = sessionsById[sessionId]!
                var title = "\(sessionId) - \(session.shortTitle)"
                let action = UIAlertAction(title: title, style: .Default) { action -> Void in
                    self.setCurrentSession(session)
                }
                alertController.addAction(action)
            }
            
            alertController.popoverPresentationController?.sourceView = sender as UIView;
            
            //show the AlertController
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    private func setCurrentSession(session: EliveSession) {
        currentSession = session
        userChoosenSession = true
        updateViewFromData()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = attendanceList != nil ? attendanceList!.count : 0

        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var attendeeCell:UITableViewCell = self.attendanceTableView.dequeueReusableCellWithIdentifier("attendee") as UITableViewCell
        
        var nameLabel = attendeeCell.viewWithTag(1) as UILabel
        var dateTimeLabel = attendeeCell.viewWithTag(2) as UILabel
        
        let attendance = attendanceList![indexPath.row] as NSDictionary
        nameLabel.text = attendance["attendeeName"] as? String
        //nameLabel.numberOfLines = 1
        //nameLabel.adjustsFontSizeToFitWidth = true
        //nameLabel.minimumScaleFactor = 0.75
        
        var arrivalDate = iso8601Formatter.dateFromString(attendance["arrivalDate"] as String)
        dateTimeLabel.text = displayDateFormater.stringFromDate(arrivalDate!)
        
        return attendeeCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // ignore
    }
    
    private func startRefreshDataWithInterval(timeInterval: Double) {
        refreshDataOnce()
        refreshTimer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: Selector("refreshDataOnce"), userInfo: nil, repeats: true)
        NSLog("Started the refresh data timer")
    }
    
    private func stopRefreshData() {
        if let timer = refreshTimer {
            timer.invalidate()
            NSLog("Stopped the refresh data timer")
        }
    }
    
    private func updateViewFromData() {
        if attendanceData != nil {
            var pickLastestAsCurrent = currentSession == nil
            let now = NSDate()

            if !userChoosenSession && !pickLastestAsCurrent {
                // has enough time transpired since last picking session to switch it?
                let transpired = now.timeIntervalSinceDate(currentSessionPickedTime!)
                pickLastestAsCurrent = transpired >= (15.0 * 60.0) // 15 minutes
            }
            
            // which session are we showing?
            if pickLastestAsCurrent {
                // pick first in attendanceData
                let id = attendanceData![0]["sessionId"] as Int
                currentSession = SessionManager.instance.sessionsById[id]
                currentSessionPickedTime = now
            }
            
            if currentSession != nil {
                // filter view list to just show current session
                let sessionPredicate = NSPredicate(format: "sessionId == %d", currentSession!.sessionId)!
                attendanceList = attendanceData?.filteredArrayUsingPredicate(sessionPredicate)
                
                countLabel.text = "\(attendanceList!.count)"
                locationLabel.text = currentSession?.location
                sessionButton.setTitle("\(currentSession!.sessionId)", forState: UIControlState.Normal)
                sessionTitleButton.setTitle(currentSession!.shortTitle, forState: UIControlState.Normal)
                self.attendanceTableView.reloadData()
            }
        }
    }
    
    @objc
    private func refreshDataOnce() {
        ServerService.instance.getJsonData("attendance", action: "", id: "", completionHandler: { (jsonData: AnyObject?, error: NSError?) -> Void in
            if let data = jsonData as? NSArray {
                self.attendanceData = data

                dispatch_async(dispatch_get_main_queue(), {
                    self.updateViewFromData()
                })
            }
        })
    }
}

