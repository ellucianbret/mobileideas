//
//  EliveSessionData.swift
//  MobileIdeas
//
//  Created by Jay Glose on 3/8/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation

let sunday = "2015-04-12";
let monday = "2015-04-13";
let tuesday = "2015-04-14";
let wednesday = "2015-04-15";

var dateFormatter = NSDateFormatter()

func formatDate(dateIn: NSString) -> NSDate {
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    return dateFormatter.dateFromString(dateIn)!
}

let eliveSessionData = [
    // ***************************
    // SUNDAY
    // ***************************
    EliveSession(
        title: "Mobile and Portal Track Kickoff",
        sessionId: 14511,
        location: "Room 350 & 351",
        sessionDesc: "This overall Ellucian Mobile and Portal Kick-off session will review the overall product strategies, product development plans Luminis, Ellucian Portal, and Ellucian Mobile. Come hear some of the exiting news we have to share about the future direction of these solutions and how we’re committed to making them easier to use and support while we advance the capabilities of each. This session will be followed by separate in-depth product sessions for each of these solutions.",
        sessionDateTime: formatDate(sunday+" 13:30"))
    ,
    EliveSession(
        title: "Ellucian Mobile Roadmap and Kickoff",
        sessionId: 12892,
        location: "Room 243",
        sessionDesc: "This session covers the future versions of Ellucian Mobile and lays out the multi-year plan.",
        sessionDateTime: formatDate(sunday+" 15:00"))
    ,
    EliveSession(
        title: "Ellucian Mobile Solution Showcase",
        sessionId: 12936,
        location: "Exhibit Hall",
        sessionDesc: "Come see the exciting customizations fellow clients have created, and vote on the one you like best!",
        sessionDateTime: formatDate(sunday+" 15:30"))
    ,
    EliveSession(
        title: "Creating the Ultimate Student Mobile Experience",
        shortTitle: "Creating the Ultimate Mobile UX",
        sessionId: 12081,
        location: "Room 243",
        sessionDesc: "Student engagement and success are key concerns in the higher education community. Join this session and learn how the University of San Diego is using the Ellucian Mobile Platform to tackle these issues. This session will include why mobile should be at the heart of your communication strategy, transactions and student involvement.",
        sessionDateTime: formatDate(sunday+" 16:30"))
    ,
    // ***************************
    // MONDAY
    // ***************************
    EliveSession(
        title: "Ellucian Mobile Platform iOS Customization - Make Ellucian GO Your University's Mobile Application.",
        shortTitle: "Ellucian Mobile iOS Customization",
        sessionId: 12473,
        location: "Room 243",
        sessionDesc: "Interactive walk through the platform dev guide with the end result being a branded iOS app.",
        sessionDateTime: formatDate(monday+" 09:30"))
    ,
    EliveSession(
        title: "How Five Institutions are using Ellucian Mobile for Colleague",
        shortTitle: "How 5 are using Ellucian Mobile",
        sessionId: 13046,
        location: "Room 242",
        sessionDesc: "Five Institutions, Becker College, Brookdale Community College, Burlington County College, Mount Saint Mary’s University and Saint Peter’s University discuss how they are using Ellucian Mobile for Colleague. Topics to be covered include technical challenges, site acceptance of mobile, feedback received, implementation of new functionality, and perceived value. Audience questions will be taken.",
        sessionDateTime: formatDate(monday+" 09:30"))
    ,
    EliveSession(
        title: "Ellucian Mobile: Ask The Client Experts Open Forum",
        shortTitle: "Ellucian Mobile: Client Experts",
        sessionId: 11630,
        location: "Room 243",
        sessionDesc: "It’s a mobile world, and we just live in it. Our expert panel of Ellucian Mobile clients will share their experiences putting Mobile in the hands of their students. Also, bring your questions to ask the panel. This session is for new, experienced, and even those looking to implement Ellucian Mobile.",
        sessionDateTime: formatDate(monday+" 11:00"))
    ,
    EliveSession(
        title: "Make the Most of the Ellucian Mobile Platform",
        shortTitle: "Make the Most of the Ellucian Mobile",
        sessionId: 12494,
        location: "Room 243",
        sessionDesc: "Seton Hall University leveraged Ellucian's flexible mobile framework to: create custom APIs for the SHUMobile app, the Luminis Portal and the university's web pages; allow students to share social media information, create student engagement and launch native iOS and Android applications from the mobile app by extending the mobile app configuration; manage upgrades to the Ellucian Mobile Platform.",
        sessionDateTime: formatDate(monday+" 13:30"))
    ,
    EliveSession(
        title: "Deploying Ellucian Mobile Server in the Cloud",
        shortTitle: "Deploying Ellucian Mobile in Cloud",
        sessionId: 12659,
        location: "Room 204",
        sessionDesc: "We will discuss the pros, cons and requirements for deploying Ellucian Mobile Server in the cloud. We will discuss Ellucian Application Hosting and walk through the features and benefits of using Ellucian to host the Ellucian Mobile Server.",
        sessionDateTime: formatDate(monday+" 13:30"))
    ,
    EliveSession(
        title: "Ellucian Mobile: What's New Since Last Year?",
        shortTitle: "Ellucian Mobile: What's New?",
        sessionId: 12893,
        location: "Room 243",
        sessionDesc: "The Ellucian Mobile team put out five releases last year. This session will walk attendees through the changes made in the last year.",
        sessionDateTime: formatDate(monday+" 15:30"))
    ,
    EliveSession(
        title: "Adding a Custom SIS Module for Ellucian Mobile",
        shortTitle: "Adding a Custom SIS Module",
        sessionId: 11604,
        location: "Room 241",
        sessionDesc: "This session will go through the technical process of creating and adding a custom module using the mobile server to return student account receivable information to a mobile device shown in Ellucian Mobile using shared code with the Ellucian Shared Code Repository.",
        sessionDateTime: formatDate(monday+" 15:30"))
    ,
    EliveSession(
        title: "Ellucian Mobile: Meet the Experts!",
        sessionId: 12895,
        location: "Room 241",
        sessionDesc: "In this session, Ellucian Mobile experts will be available for informal discussions with attendees.",
        sessionDateTime: formatDate(monday+" 16:45"))
    ,
    EliveSession(
        title: "Setting up Ellucian Mobile for Colleague in an Afternoon",
        shortTitle: "Ellucian Mobile in an Afternoon",
        sessionId: 12306,
        location: "Room 342",
        sessionDesc: "This session will cover all you need to know to setup Ellucian Mobile for Colleague in an afternoon, including installation of Colleague webApi, installation and configuration of Tomcat, securing Tomcat with a SSL certificate, deploying the Mobile server .war file, editing the configuration file, setting up modules in the Cloud Configuration tool, and testing and deploying the Mobile application.",
        sessionDateTime: formatDate(monday+" 16:45"))
    ,
    // ***************************
    // TUESDAY
    // ***************************
    EliveSession(
        title: "Ellucian Mobile Platform Android Customization - Make Ellucian GO Your University's Mobile Application",
        shortTitle: "Ellucian Mobile Android Customization",
        sessionId: 12474,
        location: "Room 243",
        sessionDesc: "Interactive walk through the platform dev guide with the end result being a branded Android app.",
        sessionDateTime: formatDate(tuesday+" 10:00"))
    ,
    EliveSession(
        title: "Tightly integrating Luminis 5 and Ellucian Mobile",
        shortTitle: "Luminis 5 and Ellucian Mobile",
        sessionId: 12337,
        location: "Room 243",
        sessionDesc: "Learn how the University of Lethbridge launched Luminis 5, then shortly thereafter launched a tightly integrated mobile app using Ellucian mobile. We’ll discuss how each product was enhanced and launched, the positive feedback received, as well as how we are using a 'develop once’ approach to deploying features to Luminis, iOS and Android users simultaneously.",
        sessionDateTime: formatDate(tuesday+" 11:30"))
    ,
    EliveSession(
        title: "At the Intersection of Web, Mobile and Portal—Developing a Coordinated Strategy",
        shortTitle: "Developing a Coordinated Strategy",
        sessionId: 13455,
        location: "Room 241",
        sessionDesc: "At Wor-Wic Community College we are in the process of implementing a new content management system, a major website redesign, Ellucian Mobile and an upgrade to our Ellucian Portal. Our goal is to meet our students’ needs, and establish a consistent user experience in design and functionality. We would like to discuss our goals and challenges and share our experience in connecting the dots.",
        sessionDateTime: formatDate(tuesday+" 11:30"))
    ,
    EliveSession(
        title: "How to transform Your Mobile Application with Ellucian Mobile",
        shortTitle: "How to transform Your Mobile Application",
        sessionId: 11460,
        location: "Room 238",
        sessionDesc: "Indiana University of Pennsylvania (IUP) needed to greatly improve its mobile application. After a careful selection process, Ellucian Mobile was chosen. See how quickly and easily IUP was able to transform their mobile application, and find out why Ellucian Mobile could be the right choice for your institution.",
        sessionDateTime: formatDate(tuesday+" 14:00"))
    ,
    EliveSession(
        title: "Ellucian Mobile Platform - Staying Current with GIT",
        shortTitle: "Staying Current with GIT",
        sessionId: 12933,
        location: "Room 241",
        sessionDesc: "Learn how to use Ellucian's GIT repository for Ellucian Mobile. You'll learn effective strategies for managing your custom code changes, and how to merge Ellucian's changes into your custom application.",
        sessionDateTime: formatDate(tuesday+" 14:00"))
    ,
    EliveSession(
        title: "Google Analytics in Ellucian Mobile",
        sessionId: 12465,
        location: "Room 241",
        sessionDesc: "Ellucian GO and the Ellucian Mobile platform contains built-in support to use Google Analytics SDK. This session will cover the type of data that is collected about the demographics and usage of the Ellucian Mobile applications. It will cover a tour of the dashboards that displays this data. It also covers the advanced technical topics of how custom code should call the API and advanced queries.",
        sessionDateTime: formatDate(tuesday+" 16:00"))
    ,
    EliveSession(
        title: "Send Free Real Time Messages to Mobile with the Mobile Notification API",
        shortTitle: "Send Free Real Time Messages",
        sessionId: 12654,
        location: "Room 243",
        sessionDesc: "In this session we will talk through and demonstrate the steps needed to integrate a Web Application with the Ellucian Mobile Notification API. This will allow the sending of free real time messages to devices running Ellucian GO or Custom application. Any Web Application in which you can add code to make HTTP Post requests is a candidate",
        sessionDateTime: formatDate(tuesday+" 16:00"))
    ,
    // WEDNESDAY
    EliveSession(
        title: "Achieve Deep Web-App Integration with Ellucian Mobile JavaScript",
        shortTitle: "Deep Web-App Integration with Mobile JavaScript",
        sessionId: 12191,
        location: "Room 356",
        sessionDesc: "Ellucian Mobile 3.8 delivered a simple JavaScript library that you can use from any Web Module to achieve deeper integration with your home-grown web applications. We will walk through the methods and put them to use as we discuss a common use case; Pre-Registration Acknowledgments. This requires NO modifications to Ellucian Mobile and is ready to use for both Platform & Application sites.",
        sessionDateTime: formatDate(wednesday+" 09:45"))
    ,
    EliveSession(
        title: "Big Ideas: Passbook, Beacons, Wearables, QR Codes",
        shortTitle: "Ellucian Mobile Big Ideas",
        sessionId: 12690,
        location: "Room 350 & 351",
        sessionDesc: "iBeacon and other proximity beacons offer triggers and location augmentation that can be used on campus and in the class room. Such as campus tours, instructor sharing information directly to student devices, This session will describe and/or demonstrate uses for proximity beacons in higher education. Will include time for Q/A, brain storming and customer presentation/demos.",
        sessionDateTime: formatDate(wednesday+" 12:15"))
]