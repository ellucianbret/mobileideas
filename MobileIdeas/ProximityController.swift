//
//  ProximityController.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/4/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit
import CoreLocation


class ProximityController: UITableViewController, ProximityChangeDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clearsSelectionOnViewWillAppear = true
        
        let beaconManager = BeaconManager.instance
        beaconManager.addProximityChangeDelegate("Proximity", delegate: self, uuid: nil, major: BeaconManager.const.majorProximity)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    class func imageForProximity( proximity: CLProximity ) -> UIImage! {
        var image: UIImage! = nil;
        
        switch (proximity) {
        
        case CLProximity.Unknown:
            image = UIImage(named: "unknown")
        case CLProximity.Far:
            image = UIImage(named: "far")
        case CLProximity.Near:
            image = UIImage(named: "near")
        case CLProximity.Immediate:
            image = UIImage(named: "immediate")
        
        }
        
        return image
    }
    
    
    // MARK: - UITableViewDataSource Methods

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return mobileTeamData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("beaconCell", forIndexPath: indexPath) as ProximityTableViewCell

        let beacon = mobileTeamData[indexPath.row]
        cell.nameLabel.text = beacon.name
        cell.titleLabel.text = beacon.title
        
        cell.proximityImageView.image = ProximityController.imageForProximity(beacon.proximity)        
        cell.photoImageView.image = UIImage(named: beacon.photoName)
        cell.foundImageView.image = beacon.found ? UIImage(named: "FoundPerson") : UIImage(named: "LostPerson")
        

        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        let indexPath = self.tableView.indexPathForSelectedRow()!
        
        var controller  = segue.destinationViewController.topViewController as ProximityDetailController
        let beacon = mobileTeamData[indexPath.row]
        controller.title = beacon.name
        controller.location = beacon
    }
    
    // MARK: - ProximityDelegate Methods
    
    func proximityUpdate(beacon :CLBeacon) {
        
        if let teamBeacon = BeaconManager.instance.teamBeaconsByID[Beacon.id(beacon)] {
            if beacon.minor == teamBeacon.minor {
                teamBeacon.proximity = beacon.proximity
                
                if beacon.proximity == CLProximity.Near || beacon.proximity == CLProximity.Immediate {
                    
                    // Set the beacon to found
                    teamBeacon.found = true;
                }
                    
                // Need to be sure we redraw on the main thread.
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    func proxmityNowOutOfRange(beacon: Beacon) {
        // out of range needs to show unknown proximity - won't get a proximity update for this one
        if let teamBeacon = BeaconManager.instance.teamBeaconsByID[beacon.id()] {
            if beacon.minor == teamBeacon.minor {
                teamBeacon.proximity = CLProximity.Unknown
                
                // Need to be sure we redraw on the main thread.
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                })
            }
        }
    }

}
