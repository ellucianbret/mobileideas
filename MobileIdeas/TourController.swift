//
//  TourController.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/4/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit
import CoreLocation


class TourController: UITableViewController, ProximityChangeDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clearsSelectionOnViewWillAppear = true
        
        let beaconManager = BeaconManager.instance
        beaconManager.addProximityChangeDelegate("TourController", delegate: self, major: BeaconManager.const.majorTour)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - UITableViewDataSource Methods

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return tourLocationData.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("beaconCell", forIndexPath: indexPath) as ProximityTableViewCell

        let beacon = tourLocationData[indexPath.row]
        cell.nameLabel.text = beacon.name
        
        cell.titleLabel.text = beacon.found ? beacon.title : "Unknown"
        
        cell.proximityImageView.image = ProximityController.imageForProximity(beacon.proximity)        
        cell.photoImageView.image = UIImage(named: beacon.photoName)
        cell.foundImageView.image = beacon.found ? UIImage(named: "FoundLocation") : UIImage(named: "LostLocation")
        
        cell.userInteractionEnabled = beacon.found
        

        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        let indexPath = self.tableView.indexPathForSelectedRow()!
        
        var controller  = segue.destinationViewController.topViewController as TourDetailController
        let beacon = tourLocationData[indexPath.row]
        controller.title = beacon.name
        controller.location = beacon
    }
    
    // MARK: - ProximityDelegate Methods
    
    func proximityUpdate(beacon :CLBeacon) {
        
        // Find the beacon in the list that has been updated and update its proximity.
        if let locationBeacon = BeaconManager.instance.tourBeaconsByID[Beacon.id(beacon)] {
            locationBeacon.proximity = beacon.proximity
            
            if ( !locationBeacon.found && (beacon.proximity == CLProximity.Near || beacon.proximity == CLProximity.Immediate) ) {
                // Set the beacon to found
                locationBeacon.found = true;
                displayFoundAlertFor(locationBeacon)
            }
            
            // Need to be sure we redraw on the main thread.
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
            })
        }
    }
    
    func proxmityNowOutOfRange(beacon: Beacon) {
        // out of range needs to show unknown proximity - won't get a proximity update for this one
        if let locationBeacon = BeaconManager.instance.tourBeaconsByID[beacon.id()] {
            if beacon.minor == locationBeacon.minor {
                locationBeacon.proximity = CLProximity.Unknown
                
                // Need to be sure we redraw on the main thread.
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                })
            }
        }
    }

    
    private func displayFoundAlertFor( beacon: Beacon ) {
        dispatch_async(dispatch_get_main_queue(), {
            var alertView = UIAlertView.init(title: beacon.name,
                message: "You have found the \(beacon.title)",
                delegate: nil,
                cancelButtonTitle: "OK")
            
            alertView.show()
        })
        
    }
    

}
