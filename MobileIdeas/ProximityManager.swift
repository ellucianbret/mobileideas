//
//  ProximityManager.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/23/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


class ProximityManager: NSObject, ProximityChangeDelegate, NotificationDelegate {
    
    // Returns the singleton instance of ProximityManager
    class var instance: ProximityManager {
        struct Singleton {
            static let instance = ProximityManager()
        }
        
        return Singleton.instance
    }
    
    struct const {
        static let blockedBeaconsKey: String = "blocked_beacons"
        static let minimumTimeBetweenNotificaitons = 60.0 * 30.0 // 30 minutes
        static let notificationSoundName = "lightbulb.caf"
    }

    
    let categoryId = "proximityNotifs"
    let cancelActionId = "Proximity-Cancel"
    
    // Beacon Ids the user has chosen to block local notifications for - cached from NSDefaults
    var blockedBeaconIds = [String]()
    
    func start() {
        BeaconManager.instance.addProximityChangeDelegate("ProximityManager", delegate: self, uuid: nil, major: BeaconManager.const.majorProximity)
        NotificationManager.instance.addNotificationDelegate("ProximityManager", delegate: self)

        
        // Fetch beacon IDs the user has chosen to block local notifications for.
        let defaults = NSUserDefaults.standardUserDefaults()
        let defaultsBlockedBeaconIds = defaults.objectForKey(const.blockedBeaconsKey) as? [String]
        if defaultsBlockedBeaconIds != nil {
            blockedBeaconIds = defaultsBlockedBeaconIds!
            NSLog("Read blocked beacon ids \(blockedBeaconIds)")
        }
    }
    
    func proximityNowInRange(beacon: CLBeacon) {
        // fire a local notification

        let beaconId = Beacon.id(beacon)

        // see if local notifications for this beacon have been blocked
        if !contains(blockedBeaconIds, beaconId) {
            let theBeacon = BeaconManager.instance.teamBeaconsByID[beaconId]
            
            // has enough time transpired since last notification?
            let now = NSDate()
            if theBeacon != nil && (theBeacon!.lastNotified == nil || now.timeIntervalSinceDate(theBeacon!.lastNotified!) >= const.minimumTimeBetweenNotificaitons) {
                /* Pass beaconId in notfication's userInfo */
                let userInfo = [
                    "beaconID" : beaconId
                ]
                
                NotificationManager.instance.sendLocalNotification("\(theBeacon!.name) is near you\nUse Mobile Ideas Proximity to find them",
                    soundName: const.notificationSoundName, category: categoryId, userInfo: userInfo)
                theBeacon!.lastNotified = now
            } else {
                NSLog("Not enough time has transpired to notify for \(beaconId)")
                /*
                NotificationManager.instance.sendLocalNotification("Not enough time has transpired to notify for \(beaconId)",
                    category: categoryId, userInfo: nil)
                */
            }
        }  else {
            NSLog("Proximity notification have been blocked for beacon: \(beaconId)")
        }
    }
    
    func proxmityNowOutOfRange(beacon: CLBeacon) {
        //NotificationManager.instance.sendLocalNotification("Beacon out of range", playSound: true)
    }
    
    // define the notification categories for proximity local notifications
    func getNotificationCategories() -> NSSet {
        let category = UIMutableUserNotificationCategory()
        category.identifier = categoryId
        let cancelAction = NotificationManager.instance.buildAction(cancelActionId, title: "Hide alerts from me")
        category.setActions([cancelAction], forContext: UIUserNotificationActionContext.Default)
        let categories = NSSet(array: [category])
        return categories
    }
    
    func handleLocalNotificationAction(application: UIApplication, identifier: String?, notification: UILocalNotification) {
        NSLog("Proximity Notification handle identifier : \(identifier)")
        // User wants to block a specific beacon from causing more local notifications
        // Store blocked Beacon IDs in NSUserDefaults.
        if identifier! == cancelActionId {
            if let blockedBeaconId = notification.userInfo!["beaconID"] as? String {
                NSLog("blocked beacon id: \(blockedBeaconId)")
                // add beaconId to list of blocked beacons 
                if !contains(blockedBeaconIds, blockedBeaconId) {
                    blockedBeaconIds.append(blockedBeaconId)

                    // save it to defaults
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(blockedBeaconIds, forKey: const.blockedBeaconsKey)
                }
            }
        }
    }
    
}