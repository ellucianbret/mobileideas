//
//  NotificationManager.swift
//  MobileIdeas
//
//  Created by Bret Hansen on 3/24/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class NotificationManager: NSObject {
    
    // Returns the singleton instance of NotificationManager
    class var instance: NotificationManager {
        struct Singleton {
            static let instance = NotificationManager()
        }
        
        return Singleton.instance
    }
    
    private var started = false
    private var notificationDelegates = [String: NotificationDelegate]()
    private var alertShowing = false
    private var alertQueue = [QueuedAlert]()
    
    private var audioPlayer: AVAudioPlayer?
    
    // private init for single
    override private init() {}
    
    class func start() {
        instance.start()
    }

    func start() {
        // make sure this only happens once
        if !started {
            // start Notification Managers
            let sessionDelegate = SessionManager.start().getDelegate()
            
            let sessionCategories = sessionDelegate.getNotificationCategories?()
            let proximityCategories = ProximityManager.instance.getNotificationCategories()
            let allCategories = sessionCategories?.setByAddingObjectsFromSet(proximityCategories)
            
            // Setup Notifications
            if UIApplication.sharedApplication().respondsToSelector("registerUserNotificationSettings:") {
                UIApplication.sharedApplication().registerUserNotificationSettings(
                    UIUserNotificationSettings(
                        forTypes: UIUserNotificationType.Alert | UIUserNotificationType.Sound,
                        categories: allCategories
                    )
                )
            }
            started = true            
        }
    }
    
    func addNotificationDelegate(delegateKey: String, delegate: NotificationDelegate) {
        notificationDelegates[delegateKey] = delegate
    }
    
    class func didRegisterUserNotificationSettings(application: UIApplication, notificationSettings: UIUserNotificationSettings) {
        instance.didRegisterUserNotificationSettings(application, notificationSettings: notificationSettings)
    }
    
    func didRegisterUserNotificationSettings(application: UIApplication, notificationSettings: UIUserNotificationSettings) {
        // let delegates know
        for (key, delegate) in notificationDelegates {
            delegate.notificationsReady?(self)
        }
    }
    
    class func handleLocalNotificationAction(application: UIApplication, identifier: String?, notification: UILocalNotification) {
        instance.handleLocalNotificationAction(application, identifier: identifier, notification: notification)
    }
    
    func handleLocalNotificationAction(application: UIApplication, identifier: String?, notification: UILocalNotification) {
        // pass to delegates to handle
        for (key, delegate) in notificationDelegates {
            delegate.handleLocalNotificationAction?(application, identifier: identifier, notification: notification)
        }
    }

    class func handleDidReceiveLocalNotificationWhileActive(application: UIApplication, notification: UILocalNotification) {
        instance.handleDidReceiveLocalNotificationWhileActive(application, notification: notification)
    }
    
    func handleDidReceiveLocalNotificationWhileActive(application: UIApplication, notification: UILocalNotification) {
        // pass to delegates to handle
        for (key, delegate) in notificationDelegates {
            delegate.handleDidReceiveLocalNotificationWhileActive?(application, notification: notification)
        }
    }

    func sendScheduledLocalNotification(fireDate: NSDate, alertBody: String?=nil, soundName: String?=nil, category: String?=nil, timeZone: NSTimeZone?=nil, hasAction: Bool?=false, userInfo: [NSObject: AnyObject]?=nil) {
        
        var notification = UILocalNotification()
        notification.soundName = soundName
        notification.category = category
        notification.timeZone = timeZone
        if hasAction != nil { notification.hasAction = hasAction! }
        notification.fireDate = fireDate
        notification.alertBody = alertBody
        notification.userInfo = userInfo
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }

    func sendLocalNotification(message: String!, soundName: String?=nil, category: String?=nil, userInfo: [NSObject: AnyObject]?=nil) {
        let application = UIApplication.sharedApplication()
        if application.applicationState == UIApplicationState.Active {
            // show as an alert
            showLocalNotificationAsAlert(message, soundName: soundName)
        } else {
            let notification:UILocalNotification = UILocalNotification()
            notification.alertBody = message
            notification.category = category
            notification.userInfo = userInfo
            
            notification.soundName = soundName
            
            application.scheduleLocalNotification(notification)
        }
    }

    private class QueuedAlert: NSObject {
        var message: String
        var soundName: String?
        
        init(message: String, soundName: String?) {
            self.message = message
            self.soundName = soundName
        }
    }
    
    func showLocalNotificationAsAlert(message: String!, soundName: String?) {
        if alertShowing {
            alertQueue.insert(QueuedAlert(message: message, soundName: soundName), atIndex: 0)
        } else {
            doShowLocalNotificationAsAlert(message, soundName: soundName)
        }
    }
    
    private func doShowLocalNotificationAsAlert(message: String!, soundName: String?) {
        let application = UIApplication.sharedApplication()
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        let okayAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in
            if self.alertQueue.count > 0 {
                let queued = self.alertQueue.removeLast()
                self.doShowLocalNotificationAsAlert(queued.message, soundName: queued.soundName)
            } else {
                self.alertShowing = false
            }
        }
        alertController.addAction(okayAction)
        
        alertShowing = true
        application.keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
        
        if (soundName != nil && !(soundName!.isEmpty)) {
            let path = NSBundle.mainBundle().pathForResource(soundName?.stringByDeletingPathExtension, ofType: soundName?.pathExtension)
            let url = NSURL(fileURLWithPath: path!)
            var error: NSError?
            audioPlayer = AVAudioPlayer(contentsOfURL: url!, error: &error)
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        }
    }
    
    func buildAction(id: String, title: String) -> UIMutableUserNotificationAction {
        let okayAction = UIMutableUserNotificationAction()
        okayAction.identifier = id
        okayAction.title = title
        okayAction.activationMode = UIUserNotificationActivationMode.Background
        okayAction.destructive = false
        okayAction.authenticationRequired = false
        return okayAction
    }
    
}