//
//  TourDetailController.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/19/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit
import CoreLocation

class TourDetailController: UIViewController, UIWebViewDelegate, ProximityChangeDelegate {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var foundImageView: UIImageView!
    @IBOutlet weak var locationDetails: UIWebView!
    
    var location : Beacon!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        // Set the delegate for the webview so we can intercept link clicks and open them in the external browser.
        self.locationDetails.delegate = self
        
        // Set the photo
        self.photoView.image = UIImage(named: location.photoName+"_large") // TODO: Use the 1x/2x image set capabilities instead.

        self.locationImageView.image = ProximityController.imageForProximity(location.proximity)
        
        // Set the found image
        foundImageView.image = location.found ? UIImage(named: "FoundLocation") : UIImage(named: "LostLocation")
        
        // Set the HTML Content
        let htmlString = "<strong>Location:</strong> \(location.title)<br /> \(location.content)"
        self.locationDetails.loadHTMLString(htmlString, baseURL: nil)
        
        BeaconManager.instance.addProximityChangeDelegate("TourDetail", delegate: self, uuid: nil, major: BeaconManager.const.majorTour)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UIWebviewDelegate Methods
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if (navigationType == UIWebViewNavigationType.LinkClicked) {
            
            UIApplication.sharedApplication().openURL(request.URL)
            return false;
        }
        return true;
    }
    
    // MARK: - ProximityDelegate Methods
    
    func proximityUpdate(beacon :CLBeacon) {
        
        // Find the beacon in the list that has been updated and update its proximity.
        if ( Beacon.id(beacon) == location.id() ) {
            location.proximity = beacon.proximity
            
            // Need to be sure we redraw on the main thread.
            dispatch_async(dispatch_get_main_queue(), {
                self.locationImageView.image = ProximityController.imageForProximity(self.location.proximity)
            })
        }
    }
    
    func proxmityNowOutOfRange(beacon: Beacon) {
        // out of range needs to show unknown proximity - won't get a proximity update for this one
        if ( beacon.id() == location.id() ) {
            location.proximity = CLProximity.Unknown
            
            // Need to be sure we redraw on the main thread.
            dispatch_async(dispatch_get_main_queue(), {
                self.locationImageView.image = ProximityController.imageForProximity(self.location.proximity)
            })
        }
    }
}
