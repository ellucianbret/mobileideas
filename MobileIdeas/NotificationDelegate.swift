//
//  NotificationDelegate.swift
//  MobileIdeas
//
//  Created by Bret Hansen on 3/25/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation
import UIKit

@objc
protocol NotificationDelegate {
    // Get a the set of categories for this delegate
    optional func getNotificationCategories() -> NSSet
    
    // Called after appDelegate didRegisterUserNotificationSettings
    optional func notificationsReady(manager: NotificationManager)
    
    // called when a local notification action is requested to be handled
    optional func handleLocalNotificationAction(application: UIApplication, identifier: String?, notification: UILocalNotification)
    
    // called when a local notification happened while the application is active
    optional func handleDidReceiveLocalNotificationWhileActive(application: UIApplication, notification: UILocalNotification)
}