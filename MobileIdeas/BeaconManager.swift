//
//  BeaconManager.swift
//  MobileIdeas
//
//  Created by Bret Hansen on 3/2/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit
import CoreLocation


// MARK: - BeaconManager Class
class BeaconManager: NSObject {
    
    // MARK: Class Constants    
    struct const {
        static let eliveBeaconUUID  = "454C6976-654D-6f62-6C69-654964656173"
        static let eliveBeaconIdentifier = "ELive"
        
        static let majorAttendance = 24948
        static let majorProximity = 25965
        static let majorTour = 29539
        static let majorPassbook = 28770
        
        static let minimumUnknownTimeToFireNowOutOfRange = 60.0 * 5.0 // 5 minutes
    }

    // Returns the singleton instance of BeaconManager
    class var instance: BeaconManager {
        struct Singleton {
            static let instance = BeaconManager()
        }
        
        return Singleton.instance
    }
    
    private var started = false
    
    private var locationManager: CLLocationManager?
    private var locationManagerDelegate: LocationManagerDelegate?
    private var didStartBeaconMonitoring = false
    
    private var beaconRegion: CLBeaconRegion?
    private var proximityChangeDelegates = [String: ProximityChangeDelegateData]()
    private var attendeeName: String = ""
    
    private var inRangeTrackBeacon = [String: [String: TrackBeacon]]()
    
    let teamBeaconsByID = [String: Beacon]()
    let teamBeaconsByUUID = [String: Beacon]()

    let tourBeaconsByID = [String: Beacon]()
    let tourBeaconsByUUID = [String: Beacon]()
    
    // private init for single
    override private init() {
        for beacon in mobileTeamData {
            let beaconUUID = beacon.uuid.uppercaseString
            teamBeaconsByUUID[beaconUUID] = beacon
            let beaconId = beacon.id()
            teamBeaconsByID[beaconId] = beacon
        }

        for beacon in tourLocationData {
            let beaconUUID = beacon.uuid.uppercaseString
            tourBeaconsByUUID[beaconUUID] = beacon
            let beaconId = beacon.id()
            tourBeaconsByID[beaconId] = beacon
        }
    }
    
    func start() {
        if !started {
            locationManager = CLLocationManager()
            locationManagerDelegate = LocationManagerDelegate(beaconManager: self)
            locationManager!.delegate = locationManagerDelegate
            locationManager!.pausesLocationUpdatesAutomatically = true
            locationManager!.activityType = CLActivityType.Fitness

            if(locationManager!.respondsToSelector("requestAlwaysAuthorization")) {
                locationManager!.requestAlwaysAuthorization()
            }
            
            // Load the beacon found data from prefs
            BeaconData.loadBeaconData()

            // start beacon related managers
            AttendanceManager.instance.start()
            ProximityManager.instance.start()
            
            started = true
        }
        
        attemptToStartBeaconMonitoring()
    }
    
    private func attemptToStartBeaconMonitoring(reverify: Bool=false) {
        didStartBeaconMonitoring = reverify ? false : didStartBeaconMonitoring
        if !didStartBeaconMonitoring {
            let authorizationStatus = CLLocationManager.authorizationStatus()
            switch authorizationStatus {
            case CLAuthorizationStatus.AuthorizedAlways:
                startBeaconMonitoring()
            case CLAuthorizationStatus.AuthorizedWhenInUse:
                startBeaconMonitoring()
                warnLocationAuthorizedAlwaysNeeded()
            case CLAuthorizationStatus.Denied:
                warnLocationAuthorizedAlwaysNeeded()
            case CLAuthorizationStatus.NotDetermined:
                // may be authorized later - watch for didChangeAuthorizationStatus on delegate
                let waiting = true
            case CLAuthorizationStatus.Restricted:
                warnLocationAuthorizedAlwaysNeeded()
            }
        }
    }
    
    private func startBeaconMonitoring() {
        // monitor the ELiveMobileIdeas beacon region (Attendance and Tour)
        let beaconUUID = NSUUID(UUIDString: const.eliveBeaconUUID)!
        beaconRegion = CLBeaconRegion(proximityUUID: beaconUUID, identifier: const.eliveBeaconIdentifier)
        locationManager!.startMonitoringForRegion(beaconRegion)
        
        // monitor a beacon region per team member. This is necessary to facilitate ability to show notification
        // for subsequent team member arrival
        for beacon in mobileTeamData {
            let memberUUID = NSUUID(UUIDString: beacon.uuid)!
            let identifier = "\(const.eliveBeaconIdentifier)-\(beacon.photoName)"
            beaconRegion = CLBeaconRegion(proximityUUID: memberUUID, identifier: identifier)
            locationManager!.startMonitoringForRegion(beaconRegion)
        }
        
        didStartBeaconMonitoring = true
    }
    
    func warnLocationAuthorizedAlwaysNeeded() {
        let title = "Warning: Mobile Ideas can not fully utilize the iBeacon related features without the \"Always access your location\" permission"
        let message = "No geo location information is read nor stored. This application only needs to use the Core Location API to find iBeacons"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            dispatch_async(dispatch_get_main_queue()) {
                let opened = UIApplication.sharedApplication().openURL(NSURL(fileURLWithPath: UIApplicationOpenSettingsURLString)!)
            }
        }
        alertController.addAction(cancelAction)
        
        let application = UIApplication.sharedApplication()
        dispatch_async(dispatch_get_main_queue()) {
            let c: Void? = application.keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func appicationDidBecomeActive(application: UIApplication) {
        attemptToStartBeaconMonitoring()
    }
    
    func addProximityChangeDelegate(delegateKey: String, delegate: ProximityChangeDelegate, uuid: String?=nil, major: Int?=nil) {
        let delegateData = ProximityChangeDelegateData(delegateKey: delegateKey, delegate: delegate, major: major)
        proximityChangeDelegates[delegateKey] = delegateData
        
        // fire proximity update for any known beacons to this delegate
        for (beaconUUID, trackBeacons) in inRangeTrackBeacon {
            if uuid == nil || uuid == beaconUUID {
                for (id, trackBeacon) in trackBeacons {
                    let beacon = trackBeacon.beacon
                    if major == nil || major! == beacon.major {
                        delegate.proximityUpdate?(beacon)
                        
                        // also call the now in range
                        delegate.proximityNowInRange?(beacon)
                    }
                }
            }
        }
    }

    func removeProximityChangeDelegate(delegateKey: String) {
        proximityChangeDelegates[delegateKey] = nil
    }
    
    
    private func fireBeaconProximityUpdateToDelegates(beacon: CLBeacon) {
        let proximity = beaconProximityToString(beacon.proximity)
        NSLog("firing fireBeaconProximityUpdateToDelegates for \(beacon.proximityUUID).\(beacon.major).\(beacon.minor) proximity: \(proximity)")
        for (key, delegateData) in proximityChangeDelegates {
            if (delegateData.major == beacon.major || delegateData.major == Int.max) {
                delegateData.delegate.proximityUpdate?(beacon)
            }
        }
    }
    
    private func fireBeaconProximityNowInRangeToDelegates(beacon: CLBeacon) {
        NSLog("firing fireBeaconProximityNowInRangeToDelegates for \(beacon.proximityUUID).\(beacon.major).\(beacon.minor)")
        for (key, delegateData) in proximityChangeDelegates {
            if (delegateData.major == beacon.major || delegateData.major == Int.max) {
                delegateData.delegate.proximityNowInRange?(beacon)
            }
        }
    }
    
    private func fireBeaconProximityNowOutOfRangeToDelegates(beacon: Beacon) {
        NSLog("firing fireBeaconProximityNowOutOfRangeToDelegates for \(beacon.uuid).\(beacon.major).\(beacon.minor)")
        for (key, delegateData) in proximityChangeDelegates {
            if (delegateData.major == beacon.major || delegateData.major == Int.max) {
                delegateData.delegate.proxmityNowOutOfRange?(beacon)
            }
        }
    }
    
    func beaconProximityToString(proximity: CLProximity) -> String {
        var proximityString: String
        
        switch(proximity) {
        case .Far:
            proximityString = "Far"
        case .Near:
            proximityString = "Near"
        case .Immediate:
            proximityString = "Immediate"
        case .Unknown:
            proximityString = "Unknown"
        }
        
        return proximityString
    }
    
    func updateName() {
        let deviceApiName = "device"
        
        let deviceUUID = UIDevice.currentDevice().identifierForVendor.UUIDString
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let name = defaults.stringForKey(SettingsViewController.const.namePreferenceKey)
        
        var newAttendeeName = ""
        if name != nil {
            newAttendeeName += name!
        }

        if (newAttendeeName.isEmpty) {
            newAttendeeName = "<Unkown>"
        }
        
        if ( attendeeName != newAttendeeName ) {
        
            attendeeName = newAttendeeName

            println("AttendeeName changed: \(attendeeName)")

            let deviceData = [
                "deviceUUID": deviceUUID,
                "attendeeName": attendeeName
            ]
            ServerService.instance.postJson(deviceApiName, id: deviceUUID, data: deviceData) { (jsonData: AnyObject?, error: NSError?) -> Void in
                // noop
            }
        }
    }

    // MARK: - ProximityChangeDelegateData Class
    private class ProximityChangeDelegateData {
        let delegateKey: String
        let delegate: ProximityChangeDelegate
        let major: Int?
        
        init(delegateKey: String, delegate: ProximityChangeDelegate, major: Int?) {
            self.delegateKey = delegateKey
            self.delegate = delegate
            self.major = major
        }
    }

    
    // MARK: - TrackBeacon Class
    private class TrackBeacon: NSObject {
        let beacon: CLBeacon
        let lastRanged: NSDate
        
        init(beacon: CLBeacon, lastRanged: NSDate) {
            self.beacon = beacon
            self.lastRanged = lastRanged
        }
    }

    // MARK: - LocationManagerDelegate
    // Private class which handles the CLLocationManagerDelegate protocol functions
    private class LocationManagerDelegate : NSObject, CLLocationManagerDelegate {
        let beaconManager: BeaconManager
        
        init(beaconManager: BeaconManager) {
            self.beaconManager = beaconManager
        }
        
        func locationManager(_manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            var reverify = false
            // location authorization changed
            if status == CLAuthorizationStatus.Denied || status == CLAuthorizationStatus.AuthorizedWhenInUse {
                // re-attempt to start monitoring - re-enables alerts
                reverify = true
            }
            
            beaconManager.attemptToStartBeaconMonitoring(reverify: reverify)
        }

        func locationManager(manager: CLLocationManager!,
            didStartMonitoringForRegion region: CLRegion!) {
                NSLog("didStartMonitoringForRegion: \(region.identifier)")
                
                // request current state to pick up beacons already with in range
                manager.requestStateForRegion(region)
        }
        
        func locationManager(manager: CLLocationManager!,
            didDetermineState state: CLRegionState,
            forRegion region: CLRegion!) {
                let stateString = state == CLRegionState.Inside ? "Inside" : "Outside"
                NSLog("didDetermineState: \(stateString) \(region.identifier)")
                // This ensures if we start up already in a region, that ranging is started, so we can determine beacons within range
                let applicationState = UIApplication.sharedApplication().applicationState
                if state == CLRegionState.Inside && applicationState == UIApplicationState.Active {
                    manager.startRangingBeaconsInRegion(region as CLBeaconRegion)
                }
        }
        
        func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
            let beaconRegion = region as CLBeaconRegion
            NSLog("Entered the region: \(beaconRegion.proximityUUID.UUIDString)")
            if CLLocationManager.isRangingAvailable() {
                manager.startRangingBeaconsInRegion(region as CLBeaconRegion)
            }
        }
        
        func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
            let beaconRegion = region as CLBeaconRegion
            NSLog("Exited the region: \(beaconRegion.proximityUUID.UUIDString)")
            
            let beaconUUID = beaconRegion.proximityUUID.UUIDString.uppercaseString
            let beacon = beaconManager.teamBeaconsByUUID[beaconUUID]
            if beacon != nil {
                // team beacon. fire the now out of range from here, since each has a unique UUID
                beaconManager.fireBeaconProximityNowOutOfRangeToDelegates(beacon!)
                
                // remove it from the in range beacons
                let beaconId = beacon!.id()
                
                beaconManager.inRangeTrackBeacon[beaconUUID]?[beaconId] = nil
            }
            
            if CLLocationManager.isRangingAvailable() {
                manager.stopRangingBeaconsInRegion(region as CLBeaconRegion)
            }
        }
        
        func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion!) {
            // copy in range beacons so we can tell if one went out of range
            let beaconUUID = region.proximityUUID.UUIDString.uppercaseString
            var nowOutOfRangeTrackBeacons = beaconManager.inRangeTrackBeacon[beaconUUID]

            let now = NSDate()
            if beacons.count > 0 {
                var nowInRangeBeacons = [String: CLBeacon]()
                var proximityUpdateBeacons = [String: CLBeacon]()
                
                //NSLog("Ranging \(beacons.count) beacons")
                for beaconAnyObject in beacons {
                    let beacon = beaconAnyObject as CLBeacon
                    let beaconId = Beacon.id( beacon )

                    // find reference to beacon if already in range
                    let inRangeTrackBeacon = beaconManager.inRangeTrackBeacon[beaconUUID]?[beaconId]
                    
                    // remove from out of range collection
                    //NSLog("removing \(beaconId) from out of range")
                    nowOutOfRangeTrackBeacons?[beaconId] = nil
                    
                    if inRangeTrackBeacon == nil {
                        // just came in range
                        //NSLog("adding \(beaconId) to now in range")
                        nowInRangeBeacons[beaconId] = beacon
                    }
                    
                    if inRangeTrackBeacon == nil || inRangeTrackBeacon!.beacon.proximity != beacon.proximity {
                        // add to list of updates
                        //NSLog("adding \(beaconId) to proximity update")
                        proximityUpdateBeacons[beaconId] = beacon

                        // keep track of the in range beacon
                        //NSLog("updating \(beaconId) in range")
                        if beaconManager.inRangeTrackBeacon[beaconUUID] == nil {
                            beaconManager.inRangeTrackBeacon[beaconUUID] = [String: TrackBeacon]()
                        }
                        let trackBeacon = TrackBeacon(beacon: beacon, lastRanged: now)
                        beaconManager.inRangeTrackBeacon[beaconUUID]![beaconId] = trackBeacon
                    }
                }
                
                // notify any beacons that are now in range
                for (id, beacon) in nowInRangeBeacons {
                    beaconManager.fireBeaconProximityNowInRangeToDelegates(beacon)
                }
                
                // notify any beacons proximity updates
                for (id, beacon) in proximityUpdateBeacons {
                    beaconManager.fireBeaconProximityUpdateToDelegates(beacon)
                }
            } else {
                //NSLog("Ranging but found no beacons")
            }

            // remove and notifiy for any beacons that are no longer in range
            // notify any beacons that are now out of range
            if let unwrappedNowOutOfRangeTrackBeacons = nowOutOfRangeTrackBeacons {
                for (beaconId, trackBeacon) in unwrappedNowOutOfRangeTrackBeacons {
                    let beaconUUID = trackBeacon.beacon.proximityUUID.UUIDString.uppercaseString
                    
                    // ignore team beacons - this will be handled in the didExitRegion
                    if beaconManager.teamBeaconsByUUID[beaconUUID] == nil {
                        // ensure enough time has expired to fire out of range
                        let now = NSDate()
                        if now.timeIntervalSinceDate(trackBeacon.lastRanged) >= BeaconManager.const.minimumUnknownTimeToFireNowOutOfRange {
                            beaconManager.inRangeTrackBeacon[beaconUUID]?[beaconId] = nil
                            let beacon = trackBeacon.beacon
                            let ideaBeacon = Beacon(uuid: beaconUUID, major: beacon.major as Int, minor: beacon.minor as Int)
                            beaconManager.fireBeaconProximityNowOutOfRangeToDelegates(ideaBeacon)
                        }
                    }
                }
            }
        }
        
        func locationManager(manager: CLLocationManager!,
            monitoringDidFailForRegion region: CLRegion!,
            withError error: NSError!) {
                NSLog("monitoringDidFailForRegion called")
        }
    }
    
}