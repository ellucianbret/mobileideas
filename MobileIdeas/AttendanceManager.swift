//
//  AttendanceService.swift
//  MobileIdeas
//
//  Created by Bret Hansen on 3/21/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class AttendanceManager: NSObject, ProximityChangeDelegate {
    
    let attendanceApiName = "attendance"
    let iso8601Formatter = NSDateFormatter()
    let categoryId = "attendanceNotifs"
    let deviceUUIDKey = "deviceUUID"
    
    // Returns the singleton instance of BeaconManager
    class var instance: AttendanceManager {
        struct Singleton {
            static let instance = AttendanceManager()
        }
        
        return Singleton.instance
    }
    
    // private init for single
    override private init() {
        iso8601Formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        iso8601Formatter.timeZone = NSTimeZone(name: "UTC")
    }
    
    var deviceUUID: String {
        var uuid: String?
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let defaultsDeviceUUID = defaults.objectForKey(deviceUUIDKey) as? String
        if defaultsDeviceUUID != nil {
            uuid = defaultsDeviceUUID
        } else {
            uuid = UIDevice.currentDevice().identifierForVendor.UUIDString
            defaults.setObject(uuid, forKey: deviceUUIDKey)
        }
        
        return uuid!
    }
    
    func start() {
        BeaconManager.instance.addProximityChangeDelegate("Attendance", delegate: self, uuid: nil, major: BeaconManager.const.majorAttendance)
    }
    
    func proximityNowInRange(beacon: CLBeacon) {
        NSLog("Attendance Beacon \(Beacon.id(beacon)) seen in AttendanceService")
        
        var sessionId = beacon.minor
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        var attendeeName = "<Unknown>"
        if let name = defaults.stringForKey(SettingsViewController.const.namePreferenceKey) {
            if !name.isEmpty {
                attendeeName = name
            }
        }
        
        var arrivalDate = iso8601Formatter.stringFromDate(NSDate())
        
        var attendance = [
            "sessionId": sessionId,
            "attendeeName": attendeeName,
            "arrivalDate": arrivalDate,
            "deviceUUID": deviceUUID
        ]
        
        ServerService.instance.postJson(attendanceApiName, data: attendance) { (jsonData: AnyObject?, error: NSError?) -> Void in
            if let data = jsonData as? NSDictionary {
                let status = data["status"] as? String
                if status != nil && status! == "success" {
                    let session = SessionManager.instance.sessionsById[sessionId]
                    let userInfo = [ "sessionId": sessionId]
                    var message = "Thank you for attending session \(sessionId)" + (session != nil ? " - \(session!.title)" : "")
                    NotificationManager.instance.sendLocalNotification(message, soundName: "blop.mp3", category: self.categoryId, userInfo: userInfo)
                }

            }
        }
    }
}