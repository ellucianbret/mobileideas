//
//  ProximityDetailController.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/19/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit
import CoreLocation

class ProximityDetailController: UIViewController, UIWebViewDelegate, ProximityChangeDelegate {

    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var proximityImageView: UIImageView!
    @IBOutlet weak var foundImageView: UIImageView!
    @IBOutlet weak var proximityDetails: UIWebView!
    
    var location : Beacon!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        // Set the delegate for the webview so we can intercept link clicks and open them in the external browser.
        self.proximityDetails.delegate = self
        
        // Set the photo
        self.photoView.image = UIImage(named: location.photoName+"_large") // TODO: Use the 1x/2x image set capabilities instead.

        self.proximityImageView.image = ProximityController.imageForProximity(location.proximity)
        
        // Set the found image
        foundImageView.image = location.found ? UIImage(named: "FoundPerson") : UIImage(named: "LostPerson")
        
        // Set the HTML Content
        let htmlString = "<strong>Title:</strong> \(location.title)<br /> \(location.content)"
        self.proximityDetails.loadHTMLString(htmlString, baseURL: nil)
        
        BeaconManager.instance.addProximityChangeDelegate("ProximityDetail", delegate: self, uuid: nil, major: BeaconManager.const.majorProximity)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionButtonPressed(sender: AnyObject) {
        // TODO: Add to contacts action.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UIWebviewDelegate Methods
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if (navigationType == UIWebViewNavigationType.LinkClicked) {
            
            UIApplication.sharedApplication().openURL(request.URL)
            return false;
        }
        return true;
    }
    
    // MARK: - ProximityDelegate Methods
    
    func proximityUpdate(beacon :CLBeacon) {
        
        // is this the location beacon?
        if ( Beacon.id(beacon) == location.id() ) {
            location.proximity = beacon.proximity
            
            if beacon.proximity == CLProximity.Near || beacon.proximity == CLProximity.Immediate {
                
                // Set the beacon to found
                location.found = true;

                // Need to be sure we redraw on the main thread.
                dispatch_async(dispatch_get_main_queue(), {
                    self.foundImageView.image = UIImage(named: "FoundPerson")
                })
            }
            
            // Need to be sure we redraw on the main thread.
            dispatch_async(dispatch_get_main_queue(), {
                self.proximityImageView.image = ProximityController.imageForProximity(self.location.proximity)
            })
        }
    }
    
    func proxmityNowOutOfRange(beacon: Beacon) {
        // out of range needs to show unknown proximity - won't get a proximity update for this one
        if ( beacon.id() == location.id() ) {
            location.proximity = CLProximity.Unknown
            
            // Need to be sure we redraw on the main thread.
            dispatch_async(dispatch_get_main_queue(), {
                self.proximityImageView.image = ProximityController.imageForProximity(self.location.proximity)
            })
        }
    }

}
