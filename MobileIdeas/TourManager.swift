//
//  TourAlertService.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/23/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation
import CoreLocation


class TourManager: NSObject, ProximityChangeDelegate {
    
    // Returns the singleton instance of TourAlertService
    class var instance: TourManager {
        struct Singleton {
            static let instance = TourManager()
        }
        
        return Singleton.instance
    }
    
    var beaconsById = [String: Beacon]()
    
    func start() {
        BeaconManager.instance.addProximityChangeDelegate("TourAlertService", delegate: self, uuid: nil, major: BeaconManager.const.majorTour)
        
        // build the beaconsById dictionary
        for beacon in tourLocationData {
            let beaconId = beacon.id()
            beaconsById[beaconId.uppercaseString] = beacon
        }
    }
    
    func proximityNowInRange(beacon: CLBeacon) {
    }
}