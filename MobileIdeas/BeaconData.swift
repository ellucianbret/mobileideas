//
//  BeaconData.swift
//  MobileIdeas
//
//  Created by Jesse Empey on 3/4/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation

// Jesse Empey - SThX Kontakt
// Sheryl Lemma - Tan Reco
// Jason Hocker - NzpS Kontakt
// Ryan Lufkin - Pink Reco
// Bret Hansen - green Reco
// Wayne Bovier - 4h6E Kontakt


var mobileTeamData = [
    Beacon(uuid: "454C6976-654D-6f62-6C69-65426f766965", major: BeaconManager.const.majorProximity, minor: 1, found:false,
        name: "Wayne Bovier", title: "VP, Product Management", photoName: "wayne",
        content: "<strong>Email: </strong><a href=mailto:wayne.bovier@ellucian.com>Wayne.Bovier@ellucian.com</a><br/><br/>As vice president of product management, Wayne Bovier provides strategic and executive leadership for Ellucian’s Mobile, Document Management, Performance Management, and Teaching & Learning businesses and solutions that span three educational ERPs and have a global footprint in over 40 countries. He has successfully launched more than 10 major products/solutions (four new offerings), including Ellucian MobileTM, Datatel Mobile (MOX), Colleague® Student Planning, Ellucian’s award-winning Colleague® Intelligent Learning Platform, Institutional Performance Management, and the Colleague® Portal. Wayne brings more than 18 years of diverse and comprehensive experience in leading commercial software strategy, operations, and product development in the education, internet, and telecommunications industries.Wayne holds a bachelor’s degree in English from Dickinson College in Carlisle, Pennsylvania, and an MBA degree from the University of North Carolina at Chapel Hill, with a concentration in international business."),
    Beacon(uuid: "454C6976-654D-6f62-6C69-65456d706579", major: BeaconManager.const.majorProximity, minor: 2, found:false,
        name: "Jesse Empey", title: "Software Development Manager", photoName: "jesse",
        content: "<strong>Email: </strong><a href=mailto:jesse.empey@ellucian.com>Jesse.Empey@ellucian.com</a><br/><strong>Other: </strong><a href='https://www.linkedin.com/in/jesseempey' target='_blank'>LinkedIn</a>, <a href='https://twitter.com/JesseEmpey' target='_blank'>Twitter</a><br/><br/>Jesse Empey is the Software Development Manager responsible for mobile solutions at Ellucian. He has been in the software industry since 1994 developing varying solutions, from Mac 3D modeling applications, to full Java enterprise web solutions, but has focused his current efforts on all things mobile. He has led a wide array of global development teams and is well versed in the latest agile software development methodologies, receiving certifications from Net Objectives and the Scrum Alliance. Jesse is passionate about user experience and developing solid solutions that are easy to use for faculty and students"),
    Beacon(uuid: "454C6976-654D-6f62-6C69-6548616e7365", major: BeaconManager.const.majorProximity, minor: 3, found:false,
        name: "Bret Hansen", title: "Architect, Senior", photoName: "bret",
        content: "<strong>Email: </strong><a href=mailto:bret.hansen@ellucian.com>Bret.Hansen@ellucian.com</a><br/><strong>Other: </strong><a href='https://www.linkedin.com/in/brethansen' target='_blank'>LinkedIn</a>, <a href='https://twitter.com/brethansen' target='_blank'>Twitter</a><br/><br/>Bret Hansen is the Senior Architect on the Ellucian Mobile development team. He has concentrated his efforts on the Ellucian Mobile product for the past several years. Bret has a passion for helping institutions get the most important data to the students and faculty using the myriad of mobile devices that have become so much a part of our lives.Bret has been with the Ellucian organization since 2000, initially as a developer and architect for the Campus Pipeline product and company. Campus Pipeline was purchased by SCT, which became SunGard SCT, then SunGard Higher Eucation. Ultimately combining with Datatel to form Ellucian. Bret has over 22 years experience as a software developer and graduated from the University of Utah."),
    Beacon(uuid: "454C6976-654D-6f62-6C69-65486f636b65", major: BeaconManager.const.majorProximity, minor: 4, found:false,
        name: "Jason Hocker", title: "Software Developer, Principal", photoName: "jason",
        content: "<strong>Email: </strong><a href=mailto:jason.hocker@ellucian.com>Jason.Hocker@ellucian.com</a><br/><strong>Other: </strong><a href='https://www.linkedin.com/in/jasonhocker' target='_blank'>LinkedIn</a>, <a href='https://twitter.com/JasonHocker' target='_blank'>Twitter</a><br/><br/>Jason Hocker is a Principal Software Developer at Ellucian on the Mobile development team. His primary focus is on the iOS and Android applications. Jason started his career at Datatel as an intern in 2000, and has continued to work for Datatel/Ellucian. His projects have included DMI, WebAdvisor, Envision Computer Column language, Colleague Studio, and Intelligent Learning Platform. He has been working on mobile since 2008. Jason has apps published for iOS, Android, BlackBerry, and Windows Phone. He has many professional certifications, such as Microsoft Certified Professional Developer and several Java certifications. Jason is a graduate of Lebanon Valley College. He has also earned a certificate in Bracketology at St. Joseph's University and in the Ice Cream Short Course at Penn State."),
    Beacon(uuid: "454C6976-654D-6f62-6C69-654c656d6d61", major: BeaconManager.const.majorProximity, minor: 5, found:false,
        name: "Sheryl Lemma", title: "Product Owner, Principal", photoName: "sheryl",
        content: "<strong>Email: </strong><a href=mailto:sheryl.lemma@ellucian.com>Sheryl.Lemma@ellucian.com</a><br/><strong>Other: </strong><a href='https://www.linkedin.com/in/sheryllemma' target='_blank'>LinkedIn</a>, <a href='https://twitter.com/mamalemma' target='_blank'>Twitter</a><br/><br/>Sheryl Lemma is the Principal Product Owner for Ellucian Mobile, and a certified software product owner. She has been with Ellucian for nearly 15 years, serving as product owner on new product development teams such as the Ellucian Portal, MOX and Ellucian Mobile. Prior to joining Ellucian, Sheryl worked as a Colleague and Unix system administrator at Lebanon Valley College. Sheryl is a passionate advocate for clear communication, and solving the right problems with the right technology."),
    Beacon(uuid: "454C6976-654D-6f62-6C69-654c75666b69", major: BeaconManager.const.majorProximity, minor: 6, found:false,
        name: "Ryan Lufkin", title: "Director of Product Marketing", photoName: "ryan",
        content: "<strong>Email: </strong><a href=mailto:ryan.lufkin@ellucian.com>Ryan.Lufkin@ellucian.com</a><br/><strong>Other: </strong><a href='https://www.linkedin.com/in/RyanLufkin' target='_blank'>LinkedIn</a>, <a href='https://twitter.com/ryanlufkin' target='_blank'>Twitter</a><br/><br/>Ryan Lufkin is the Director of Product Marketing for Ellucian Mobile and a portfolio of other Ellucian products. Ryan has been with Ellucian for over 10 years leading the product launch and go-to-market activities of the company’s mobile and portal technologies. Ryan is a proud alumnus of the University of Utah and currently lives in Salt Lake City, Utah, where he is an avid skier and outdoor enthusiast.")
]

var tourLocationData = [
    Beacon(uuid: "454C6976-654D-6f62-6C69-654964656173", major: BeaconManager.const.majorTour, minor: 1, found:false,
        name: "Location 1", title: "Research Lab", photoName: "location1",
        content: "This fun adventure demonstrates the use of iBeacon technology.  iBeacon is an indoor positioning system, described by Apple Inc. as 'a new class of low-powered,low-cost transmitters that can notify nearby iOS 7 or 8 devices of their presence. The technology enables a smart phone or other device to perform actions when in close proximity to an iBeacon. Android 4.3 also supports beacon technology but unfortunately we didn't have time to create an Android version of this app.</br></br>You’ll use the Mobile Tour to search for 5 beacons that have been hidden throughout the conference center.  When you get close enough to one of the beacons, the application will alert you that it has been found and will unlock a clue to finding the next location.  Speaking of your next location, it is a place of learning and experience.</br></br>Good Luck!"),
    Beacon(uuid: "454C6976-654D-6f62-6C69-654964656173", major: BeaconManager.const.majorTour, minor: 2, found:false,
        name: "Location 2", title: "Learning Lab", photoName: "location2",
        content: "You’re on your way now.  But before you move on, spend some time in the Learning Lab.  The Learning Lab will allow you to test-drive Ellucian products, interact with the software, and see some of our newest modules, features, and functionality. This technology lab offers hands-on experience with many of our solutions. You may even discover some that you didn’t know existed.  After you’ve learned all that you can learn here’s a clue to the next location.  It’s a great place to hang out and rub elbows with some of Ellucian’s finest employees, network with colleagues, or just take a break."),
    Beacon(uuid: "454C6976-654D-6f62-6C69-654964656173", major: BeaconManager.const.majorTour, minor: 3, found:false,
        name: "Location 3", title: "Lounge", photoName: "location3",
        content: "Ah, the lounge.  Take some time to relax and unwind.   Catch up with some old friends or better yet make some new ones.  You’ll get a chance to ask some of those burning questions that didn’t get answered during the sessions.  Ellucian employees are on hand to help with just that.  When your ready to continue your adventure, here’s at clue.  At your next location you'll truly be tested."),
    Beacon(uuid: "454C6976-654D-6f62-6C69-654964656173", major: BeaconManager.const.majorTour, minor: 4, found:false,
        name: "Location 4", title: "Usability Testing", photoName: "location4",
        content: "Great! You’ve found the Usability Testing area.  Usability testing is an important part of the software development process.  Spend some time here helping our User Experience team improve the products you know and love.  When you are ready to move on to the next location, here’s a clue.  It’s not far from where your journey began and it’s a great place to ask questions about Ellucian products and services."),
    Beacon(uuid: "454C6976-654D-6f62-6C69-654964656173", major: BeaconManager.const.majorTour, minor: 5, found:false,
        name: "Location 5", title: "Ellucian Booth", photoName: "location5",
        content: "Congratulations! You’ve found the last beacon.  However, your adventure doesn’t end here.  Return to the Research Lab area and introduce yourself to the tall guy with the scraggly looking beard.  Hint: His name rhymes with “Bill”.   He’ll be able to answer any questions you may have about this or any of the other labs as well as offer you an opportunity for another adventure.")
    
]

class BeaconData: NSObject {
    
    class func loadBeaconData() {
        
        // Load the beacon data from userDefatuls. TODO: Manage Beacons with CoreData
        let defaults = NSUserDefaults.standardUserDefaults()
        
        for beacon in mobileTeamData {
            if let storedFound = defaults.objectForKey(beacon.id()) as? Bool {
                beacon.found = storedFound
            }
        }
        
        for beacon in tourLocationData {
            if let storedFound = defaults.objectForKey(beacon.id()) as? Bool {
                beacon.found = storedFound
            }
        }
    }
}