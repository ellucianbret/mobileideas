//
//  ServerService.swift
//  MobileIdeas
//
//  Created by Bret Hansen on 3/21/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation

class ServerService: NSObject {
    
    // Returns the singleton instance of BeaconManager
    class var instance: ServerService {
        struct Singleton {
            static let instance = ServerService()
        }
        
        return Singleton.instance
    }

    let serverBaseUrl =
    "https://mobiletest.elluciancloud.com/MobileIdeasServer/"
    /*"http://10.0.0.13:8080/MobileIdeasServer/"*/
    
    let serverApiBaseUrl: String
    
    // private init for single
    override private init() {
        serverApiBaseUrl = "\(serverBaseUrl)api/"
    }
    
    func getJsonData(apiName: String, action: String="", id: String="", completionHandler: (data: AnyObject?, error:NSError?) -> Void) {
        // create the request
        var url = "\(serverApiBaseUrl)\(apiName)"
        if (!action.isEmpty) {
            url += "/\(action)"
        }
        if (!id.isEmpty) {
            url += "/\(id)"
        }
        
        let nsUrl = NSURL(string: url)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(nsUrl!, completionHandler: {(data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
            NSLog("Task completed")
            if let responseError = error {
                completionHandler(data: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    NSLog("Response code success: 200")
                    var jsonParseError: NSError?
                    
                    var jsonResult: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonParseError)
                    if(jsonParseError != nil) {
                        // If there is an error parsing JSON, print it to the console
                        NSLog("JSON Error \(jsonParseError!.localizedDescription)")
                        completionHandler(data: nil, error: jsonParseError)
                    } else {
                        completionHandler(data: jsonResult, error: nil)
                    }
                } else {
                    var statusError = NSError(domain:"com.ellucian", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completionHandler(data: nil, error: statusError)
                }
            }
        })
        
        task.resume()
    }
    
    func postJson(apiName: String, action: String="", id: String="", data: AnyObject, completionHandler: (jsonData: AnyObject?, error: NSError?) -> Void) {
        // create the request
        var url = "\(serverApiBaseUrl)\(apiName)"
        if (!action.isEmpty) {
            url += "/\(action)"
        }
        if (!id.isEmpty) {
            url += "/\(id)"
        }
        
        var jsonData = NSJSONSerialization.dataWithJSONObject(data, options: nil, error: nil)
        
        let nsUrl = NSURL(string: url)
        let request = NSMutableURLRequest(URL: nsUrl!)
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        
        // Make the request
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(),completionHandler: {(response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            NSLog("Post completed")
            if let responseError = error {
                completionHandler(jsonData: nil, error: responseError)
            } else if let httpResponse = response as? NSHTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    NSLog("Response code success: 200")
                    var jsonParseError: NSError?
                    
                    var jsonResult: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonParseError)
                    if(jsonParseError != nil) {
                        // If there is an error parsing JSON, print it to the console
                        NSLog("JSON Error \(jsonParseError!.localizedDescription)")
                        completionHandler(jsonData: nil, error: jsonParseError)
                    } else {
                        completionHandler(jsonData: jsonResult, error: nil)
                    }
                } else {
                    var statusError = NSError(domain:"com.ellucian", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completionHandler(jsonData: nil, error: statusError)
                }
            }
        })
    }
}