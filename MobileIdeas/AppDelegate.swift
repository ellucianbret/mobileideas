//
//  AppDelegate.swift
//  MobileIdeas
//
//  Created by Bret Hansen on 2/18/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var lastVersionCheck: NSDate?
    let minTimeBetweenVersionChecks = 60.0 * 60.0 * 2 // 2 hours
    let installUrl = "https://mobiletest.elluciancloud.com/bigideas?autoinstall=true"


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // start Beacon Manager
        BeaconManager.instance.start()
        NotificationManager.start()

        // Determine which view controller to display based if they haven't set their name.
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.synchronize()
        var alreadySetNames =  defaults.boolForKey(SettingsViewController.const.namesSetPreferenceKey)
        
        if ( !alreadySetNames ) {
            var storyboard = self.window?.rootViewController?.storyboard

            var rootViewController: UIViewController = storyboard?.instantiateViewControllerWithIdentifier("settingsController") as UIViewController
            
            self.window?.rootViewController = rootViewController
            self.window?.makeKeyAndVisible()
        }
        
        // Set the version number in the user defaults.
        if let infoDictionary = NSBundle.mainBundle().infoDictionary as NSDictionary? {
            let versionNumber = infoDictionary.objectForKey("CFBundleShortVersionString") as String
            let buildNumber = infoDictionary.objectForKey("CFBundleVersion") as String
            let version = "\(versionNumber).\(buildNumber)"
            NSLog("version: \(version)")
            defaults.setObject(version, forKey: "app_version")
            defaults.synchronize()
        }
        
        // Register a listener for preference changes
        let mainQueue = NSOperationQueue.mainQueue()
        NSNotificationCenter.defaultCenter().addObserverForName("NSUserDefaultsDidChangeNotification",
            object: nil, queue: mainQueue) {
                 (notification: NSNotification!) -> Void in
                
                println("UserDefaults changed \(notification)")
                BeaconManager.instance.updateName()
            }
        
        return true
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != nil {
            NotificationManager.didRegisterUserNotificationSettings(application, notificationSettings: notificationSettings)
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method applicationWillResignActive pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        NSLog("applicationWillResignActive")
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NSLog("applicationDidEnterBackground")
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        NSLog("applicationWillEnterForeground")
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NSLog("applicationDidBecomeActive")
        BeaconManager.instance.appicationDidBecomeActive(application)
        
        checkAppVersion()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        NSLog("applicationWillTerminate")
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        NSLog("didRecieveLocalNotification \(notification.alertBody!)")
        if application.applicationState == UIApplicationState.Active {
            NotificationManager.handleDidReceiveLocalNotificationWhileActive(application, notification: notification)
        }
    }
    
    // Handle the action taken on a notification
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        NotificationManager.handleLocalNotificationAction(application, identifier: identifier, notification: notification)

        completionHandler()
    }
    
    private func checkAppVersion() {
        // has it been long enough?
        var check = true
        let now = NSDate()
        if let uwLastVersionCheck = lastVersionCheck {
            if now.timeIntervalSinceDate(uwLastVersionCheck) < minTimeBetweenVersionChecks {
                check = false
            }
        }
        
        if check {
            // remember the check
            lastVersionCheck = now
            
            ServerService.instance.getJsonData("device", action: "version", id: "") { (jsonData: AnyObject?, error: NSError?) -> Void in
                if let data = jsonData as? NSDictionary {
                    let latestVersion = data["version"] as String
                    
                    if let infoDictionary = NSBundle.mainBundle().infoDictionary as NSDictionary? {
                        let versionNumber = infoDictionary.objectForKey("CFBundleShortVersionString") as String
                        let buildNumber = infoDictionary.objectForKey("CFBundleVersion") as String
                        let thisVersion = "\(versionNumber).\(buildNumber)"
                        if latestVersion.compare(thisVersion, options: NSStringCompareOptions.NumericSearch) == NSComparisonResult.OrderedDescending {
                            self.showVersionUpdateAlert()
                        }
                    }
                }
            }
        }
    }
    
    private func showVersionUpdateAlert() {
        let application = UIApplication.sharedApplication()
        let alertController = UIAlertController(title: "Version Update", message: "There is a new verison of the Mobile Ideas application, please touch 'Update'", preferredStyle: .Alert)
        let okayAction = UIAlertAction(title: "Update", style: .Default) { action -> Void in
            let opened = UIApplication.sharedApplication().openURL(NSURL(string: self.installUrl)!)
        }
        alertController.addAction(okayAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        alertController.addAction(cancelAction)
        
        application.keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
    }
}