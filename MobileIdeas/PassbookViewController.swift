//
//  PassbookViewController.swift
//  MobileIdeas
//
//  Created by Jason Hocker on 3/21/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation
import PassKit

class PassbookViewController : UIViewController, PKAddPassesViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(!PKPassLibrary.isPassLibraryAvailable()) {
            var alert = UIAlertController(title: "Error", message: "PassKit not available", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    @IBAction func addBoardingPass(sender: AnyObject) {
        addPass("BoardingPass")
    }
    @IBAction func addCoupon(sender: AnyObject) {
        addPass("Coupon")
    }
    @IBAction func addEvent(sender: AnyObject) {
        addPass("Event")
    }
    @IBAction func addGeneric(sender: AnyObject) {
        addPass("Generic")
    }
    @IBAction func addStoreCard(sender: AnyObject) {
        addPass("StoreCard")
    }
    func addPass(name: String) {
        var filePath = NSBundle.mainBundle().pathForResource(name, ofType:"pkpass")
        var pkfile : NSData = NSData(contentsOfFile: filePath!)!
        var pass : PKPass = PKPass(data: pkfile, error: nil)
        let vc = PKAddPassesViewController(pass: pass) as PKAddPassesViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
}