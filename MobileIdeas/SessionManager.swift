//
//  SessionManager.swift
//  MobileIdeas
//
//  Created by Jay Glose on 3/23/15.
//  Copyright (c) 2015 Ellucian. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class SessionManager: NSObject, NotificationDelegate {
    
    var started = false
    
    let notifsSavedKey = "localNotifsSaved"
    
    let timeZone: NSTimeZone
    let categoryId = "sessionNotifs"
    
    let okActionId = "Session-Ok"
    let cancelActionId = "Session-Cancel"
    
    let debugMode = false
    var debugTimeIntervalSinceNow = 10.0
    let debugTimeIntervalSinceNowIncrement = 10.0
    
    let sessionsById = [NSNumber: EliveSession]()
    
    // Returns the singleton instance of NotificationManager
    class var instance: SessionManager {
        struct Singleton {
            static let instance = SessionManager()
        }
        return Singleton.instance
    }
    
    var dateFormatter = NSDateFormatter()

    // private init for single
    override private init() {
        // had to put this hear to test on debugMode
        self.timeZone = debugMode ? NSTimeZone.localTimeZone() : NSTimeZone(name: "America/Chicago")!
        
        // build the sessions by id dictionary
        for session in eliveSessionData {
            sessionsById[session.sessionId] = session
        }
    }
    
    class func start() -> SessionManager {
        instance.start()
        return instance
    }
    
    func start() {
        // make sure this only happens once
        if !started {
            
            NotificationManager.instance.addNotificationDelegate("SessionManager", delegate: self)
            started = true
        }
    }
    
    func getDelegate() -> NotificationDelegate {
        return self
    }
    
    func getNotificationCategories() -> NSSet {
        let category = UIMutableUserNotificationCategory()
        category.identifier = categoryId
        let okayAction = NotificationManager.instance.buildAction(okActionId, title: "Ok")
        let cancelAction = NotificationManager.instance.buildAction(cancelActionId, title: "Cancel")
        category.setActions([cancelAction], forContext: UIUserNotificationActionContext.Default)
        let categories = NSSet(array: [category])
        return categories
    }
    
    func notificationsReady(manager: NotificationManager) {
        // Store a boolean to UserDefaults so we know we've already scheduled
        // these notifications.
        let defaults = NSUserDefaults.standardUserDefaults()
        let notifsSaved = defaults.boolForKey(notifsSavedKey)
        if !debugMode && notifsSaved {
            return
        }
        
        let soundName = "bellTower.mp3"
        let sessions = eliveSessionData
        let today = NSDate()
        var notifCounter = 0
        
        for session in sessions {
            var notifFireDate: NSDate
            if (debugMode) {
                notifFireDate = NSDate(timeIntervalSinceNow: debugTimeIntervalSinceNow)
                debugTimeIntervalSinceNow += debugTimeIntervalSinceNowIncrement
            } else {
                notifFireDate = subtract15Mins(session.sessionDateTime)
            }
            
            let alertBody = formatDateToString(session.sessionDateTime) + " :: " + session.title + "\n" + session.location
            
            /* Additional information, user info */
            let userInfo = [
                "SessionID" : session.sessionId,
                "SessionDesc" : session.sessionDesc
            ]
            
            /* Schedule the notification */
            if (notifFireDate.compare(today) == NSComparisonResult.OrderedDescending) {
                notifCounter++
                NotificationManager.instance.sendScheduledLocalNotification(notifFireDate, alertBody: alertBody, soundName: soundName, category: categoryId, timeZone: timeZone, hasAction: true, userInfo: userInfo)
            }
        }
        NSLog("Scheduled \(notifCounter) Local Notifications")
        defaults.setBool(true, forKey: notifsSavedKey)
    }
    
    func handleLocalNotificationAction(application: UIApplication, identifier: String?, notification: UILocalNotification) {
        NSLog("Notification handle identifier : \(identifier)")
        if identifier! == cancelActionId {
            var cancelCount = 0
            if let notifications = application.scheduledLocalNotifications {
                NSLog("Found \(notifications.count) remaining in queue.")
                for pendingNotif in notifications  {
                    if pendingNotif.category == categoryId {
                        application.cancelLocalNotification(pendingNotif as UILocalNotification)
                        cancelCount++
                    }
                }
            }
            NSLog("Canceled \(cancelCount) scheduled session notifications")
        }
    }
    
    func handleDidReceiveLocalNotificationWhileActive(application: UIApplication, notification: UILocalNotification) {
        if notification.category != nil && notification.category! == categoryId {
            NotificationManager.instance.showLocalNotificationAsAlert(notification.alertBody, soundName: notification.soundName)
        }
    }
    
    func formatStringToDate(dateIn: NSString) -> NSDate {
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        return dateFormatter.dateFromString(dateIn)!
    }
    
    func formatDateToString(dateIn: NSDate) -> NSString {
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.stringFromDate(dateIn)
    }
    
    func subtract15Mins(fromDate: NSDate) -> NSDate {
        if let newDate =  NSCalendar.currentCalendar().dateByAddingUnit(NSCalendarUnit.CalendarUnitMinute, value: -15,toDate: fromDate, options: NSCalendarOptions(0)) {
            return newDate
        }
        return fromDate
    }
}